" General

syntax on

set clipboard=unnamedplus
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set number
set numberwidth=4
set relativenumber
set signcolumn=number
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set nohlsearch
set ignorecase
set smartcase
" set nowrap
set splitbelow
set splitright
set hidden
set scrolloff=8
set noshowmode
set updatetime=250
set encoding=UTF-8
set mouse=a

"let g:bracey_browser_command
"let g:mkdp_browser='wslview'

" Colors
set termguicolors


" require plugin configs
lua require('xiquelo.telescope') 
lua require( 'xiquelo.indent_blankline' )


" Mapleader
let mapleader=" "

nmap <leader>s <Plug>(easymotion-s2)
nmap <leader>nt :NERDTreeFind<CR>

nmap <leader>w :w<CR>
nmap <leader>q :q<CR>
nmap <leader>z :wq<CR>
nmap <leader>a :q!<CR>

so ~/.config/nvim/plugins.vim
