call plug#begin('~/.vim/plugged')

" Lua
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'nvim-telescope/telescope.nvim'


" Status Bar
Plug 'maximbaz/lightline-ale'
Plug 'itchyny/lightline.vim'


" Temas
"Plug 'morhetz/gruvbox'
Plug 'kyazdani42/nvim-web-devicons'


"IDE
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
let NERDTreeQuitOnOpen=1
Plug 'preservim/nerdcommenter'
Plug 'lukas-reineke/indent-blankline.nvim'

Plug 'benmills/vimux'
Plug 'christoomey/vim-tmux-navigator'


" Typing
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-surround'


" Markdown
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }


" HTML
Plug 'turbio/bracey.vim'


" Autocomplete
"Plug 'sirver/ultisnips'
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'

" Completion
"Plug 'hrsh7th/cmp-buffer'
"Plug 'hrsh7th/cmp-path'
"Plug 'hrsh7th/cmp-nvim-lua'
"Plug 'hrsh7th/cmp-nvim-lsp'
"Plug 'hrsh7th/cmp-cmdline'
"Plug 'hrsh7th/cmp-git'
"Plug 'rcarriga/cmp-dap'
"Plug 'saadparwaiz1/cmp_luasnip'
"Plug 'onsails/lspkind-nvim'
"Plug 'L3MON4D3/LuaSnip'
"Plug 'windwp/nvim-autopairs'

call plug#end()
