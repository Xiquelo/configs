# use nix
if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then . ~/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# source plugins
source ~/.zsh_plugins.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# alias
alias mii='cd /mnt/g/user/'
alias cov='cd /mnt/c/users/COVALU/Documents/covalu/'
alias reload='source ~/.zshrc'
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias update='sudo apt update && sudo apt upgrade'
alias ins='sudo apt install'
alias ls='lsd'
alias lsa='lsd -a'
alias lsal='lsd -al'
alias nv='nvim'

# git alias
alias init='git init'
alias add='git add'
alias gcm='gc -m'
alias gcam='gc -am'
alias amd='gc -ammend -m'
alias gits='git status -sb'
alias log="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias push='git push -u origin main'
alias pull='git pull'
alias branch='git branch'
alias branchd='git branch -d'
alias reset='git reset'
alias check='git checkout'
alias checkb='git checkout -b'
